/*
 * Simple firewall - command line client
 * Project for course Data Communications, Computer Networks and Protocols (PDS)
 * Faculty of Information Technology, Brno University of Technology
 *
 * Author: Jaromir Deak, xdeakj00@stud.fit.vutbr.cz
 * Date: April 2015
 */

#include <iostream>
#include <fstream>
#include <string>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>

#include "Rule.h"
#include "analyze.tab.h"

extern int yy_scan_string ( const char *str );

using namespace std;

/**
 * Nacte pravidla ze souboru
 */
void load_rules_from_file(string file_name);

/**
 * Vypise pravidla na standardni vystup
 */
void print_rules();

/**
 * Prida pravidlo do firewallu
 */
void add_rule(string rule_str);

/**
 * Odstrani pravidlo definovane pomoci id
 */
void delete_rule(int rule_id);

/**
 * Posle prikaz modulu v jadre
 */
void send_to_proc(string command_str);

/**
 * Hlavni funkce
 */
int main(int argc, char *argv[])
{
  int opt;
  string rule_str("");

  /*
   * Zpracovani prikazu z prikazove radky
   */
  while ((opt = getopt(argc, argv, "f:pa:d:")) != -1)
  {
    switch (opt)
    {
      case 'p':
        print_rules();
        break;
      case 'f':
        load_rules_from_file(string(optarg));
        break;
      case 'a':
        for(int i = optind-1; i < argc; i++)
        {
          string argument(argv[i]);

          if (argument[0] == '-') break;

          rule_str.append(argument);
          rule_str.append(" ");
        }
        add_rule(rule_str);
        break;
      case 'd':
        delete_rule(atoi(optarg));
        break;
      case 'h':
      default:
        cout << "PDS project - Simple firewall control application" << endl
             << "Usage:" << endl
             << "-a <rule>   Add rule to firewall" << endl
             << "-f <file>   Load file with rules" << endl
             << "-d <id>     Delete rule specified by id" << endl
             << "-p          Print actually used rules" << endl
             << "-h          Print help" << endl;
        break;
    }
  }

  return 0;
}

void load_rules_from_file(string file_name)
{
  std::string str;
  fstream fs;

  fs.open(file_name.c_str(), fstream::out |fstream::in | fstream::app);

  if (fs.is_open())
  {
    while (std::getline(fs, str))
    {
      add_rule(str);
    }
  }
  else
  {
    std::cout << "Error opening kernel module communication" << endl;
  }
}

void print_rules()
{
  send_to_proc(string("p"));

  char c;
  FILE *pf = fopen("/proc/pdsfw", "r");
  if (pf == NULL)
  {
    std::cout << "Error opening kernel module communication" << endl;
    return;
  }
  else
  {
    cout << "id\taction\tsrcip\t\tsrcport\tdstip\t\tdstport\tprotocol" << endl;
    while ((c = fgetc(pf)) != EOF)
    {
      cout << c;
    }
  }

  fclose(pf);
}

void add_rule(string rule_str)
{
  Rule rule;
  yy_scan_string ( rule_str.c_str() );
  yyparse(&rule);
  //cout << rule.get_rule_string() << endl;
  send_to_proc(rule.get_rule_string());
}

void delete_rule(int rule_id)
{
  char str[100];
  sprintf(str,"%d",rule_id);
  string command_str("d ");
  command_str.append(str);
  send_to_proc(command_str);
}

void send_to_proc(string command_str)
{
  fstream fs;

  //cout << "Command:" << command_str << endl;

  fs.open("/proc/pdsfw", fstream::out |fstream::in | fstream::app);
  if (fs.is_open())
  {
    fs << command_str << endl;
    fs.close();
  }
  else
  {
    std::cout << "Error opening kernel module communication" << endl;
  }
}
