%{
#include <cstdio>
#include <iostream>
#include <string>

#include "Rule.h"

using namespace std;

// stuff from flex that bison needs to know about:
extern "C" int yylex();
extern int yyparse(Rule *rule);
extern "C" FILE *yyin;

extern int yy_scan_string ( const char *str );
 
void yyerror(Rule *rule, const char *s);

%}

// Bison fundamentally works by asking flex to get the next token, which it
// returns as an object of type "yystype".  But tokens could be of any
// arbitrary data type!  So we deal with that in Bison by defining a C union
// holding each of the types of tokens that Flex could return, and have Bison
// use that union instead of "int" for the definition of "yystype":
%union {
	char *sval;
}

// define the constant-string tokens:
// define the "terminal symbol" token types I'm going to use (in CAPS
// by convention), and associate each with a field of the union:
%token <sval> NUMBER
%token <sval> ADDRESS
%token ALLOW
%token DENY
%token TCP
%token UDP
%token ICMP
%token IP
%token ANY
%token FROM
%token TO
%token SRCPORT
%token DESTPORT 


%parse-param {Rule *rule}
%%

// the first rule defined is the highest-level rule, which in our
// case is just the concept of a whole "snazzle file":

analyze:
  rule
  ;

rule:
  id action protocol srcip destip srcport destport
  ;

id: 
  NUMBER	{ rule->set_id(string($1)); }
  ;

action:
  ALLOW		{ rule->set_action(string("allow")); }
  | DENY	{ rule->set_action(string("deny")); }
  ;

protocol:
  TCP		{ rule->set_protocol(string("tcp")); }
  | UDP		{ rule->set_protocol(string("udp")); }
  | ICMP	{ rule->set_protocol(string("icmp")); }
  | IP		{ rule->set_protocol(string("ip")); }
  ;

srcip:
  FROM ADDRESS	{ rule->set_src_ip(string($2)); }
  | FROM ANY	{ rule->set_src_ip(string("*")); }
  ;

destip:
  TO ADDRESS	{ rule->set_dest_ip(string($2)); }
  | TO ANY	{ rule->set_dest_ip(string("*")); }
  ;

srcport:
  SRCPORT NUMBER	{ rule->set_src_port(string($2)); }
  |		{ rule->set_src_port(string("*")); }
  ;

destport:
  DESTPORT NUMBER	{ rule->set_dest_port(string($2)); }
  |		{ rule->set_dest_port(string("*")); }
  ;


%%
/*
void parse_rule(Rule *rule)
{
	const char *str = "10 allow tcp from 147.229.1.1 to any dst-port 80";
	cout << "parse rule func..." << endl;
	yy_scan_string ( str );
	int id;
	yyparse(&id);
	cout << "id: " << id << endl;
}

int main(int, char**) {
	// open a file handle to a particular file:
	FILE *myfile = fopen("rules", "r");
	// make sure it's valid:
	if (!myfile) {
		cout << "I can't open a.snazzle.file!" << endl;
		return -1;
	}
	// set flex to read from it instead of defaulting to STDIN:
	yyin = myfile;

	// parse through the input until there is no more:
	//do {
	//	yyparse();
	//} while (!feof(yyin));
	
	//parse_rule();
} */

void yyerror(Rule *rule, const char *s) {
	cout << "EEK, parse error!  Message: " << s << endl;
	// might as well halt now:
	exit(-1);
}
