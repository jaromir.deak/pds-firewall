/*
 * Simple firewall - command line client
 * Project for course Data Communications, Computer Networks and Protocols (PDS)
 * Faculty of Information Technology, Brno University of Technology
 *
 * Author: Jaromir Deak, xdeakj00@stud.fit.vutbr.cz
 * Date: April 2015
 */

#include "Rule.h"

using namespace std;

Rule::Rule()
{
}

Rule::~Rule()
{
}

string Rule::get_rule_string()
{
  string rule_str("a ");

  rule_str.append(id_ + " ");
  rule_str.append(action_ + " ");
  rule_str.append(protocol_ + " ");
  rule_str.append(src_ip_ + " ");
  rule_str.append(src_port_ + " ");
  rule_str.append(dest_ip_ + " ");
  rule_str.append(dest_port_);

  return rule_str;
}

void Rule::set_action(const std::string& action)
{
  action_ = action;
}

void Rule::set_dest_ip(const std::string& destIp)
{
  dest_ip_ = destIp;
}

void Rule::set_dest_port(const std::string& destPort)
{
  dest_port_ = destPort;
}

void Rule::set_id(const std::string& id)
{
  id_ = id;
}

void Rule::set_protocol(const std::string& protocol)
{
  protocol_ = protocol;
}

void Rule::set_src_ip(const std::string& srcIp)
{
  src_ip_ = srcIp;
}

void Rule::set_src_port(const std::string& srcPort)
{
  src_port_ = srcPort;
}

