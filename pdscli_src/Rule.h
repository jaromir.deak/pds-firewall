/*
 * Simple firewall - command line client
 * Project for course Data Communications, Computer Networks and Protocols (PDS)
 * Faculty of Information Technology, Brno University of Technology
 *
 * Author: Jaromir Deak, xdeakj00@stud.fit.vutbr.cz
 * Date: April 2015
 */

#ifndef RULE_H_
#define RULE_H_

#include <iostream>
#include <string>

class Rule
{
public:
  Rule();
  ~Rule();

  std::string get_rule_string();
  void set_action(const std::string& action);
  void set_dest_ip(const std::string& destIp);
  void set_dest_port(const std::string& destPort);
  void set_id(const std::string& id);
  void set_protocol(const std::string& protocol);
  void set_src_ip(const std::string& srcIp);
  void set_src_port(const std::string& srcPort);

private:
  std::string id_;
  std::string action_;
  std::string protocol_;
  std::string src_ip_;
  std::string dest_ip_;
  std::string src_port_;
  std::string dest_port_;

  //void parse_rule(std::string rule_str);
};

#endif /* RULE_H_ */
