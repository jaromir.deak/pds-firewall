%{
#include <iostream>
#include "analyze.tab.h"
using namespace std;
#define YY_DECL extern "C" int yylex()
%}
%%
[ \t\n];
"allow"   { return ALLOW; }
deny    { return DENY; }
tcp     { return TCP; }
udp     { return UDP; }
icmp    { return ICMP; }
ip      { return IP; }
any     { return ANY; }
from    { return FROM; }
to      { return TO; }
"src-port"       { return SRCPORT; }
"dst-port"       { return DESTPORT; }
[0-9]+  { yylval.sval = strdup(yytext); return NUMBER; }
(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?) { yylval.sval = strdup(yytext); return ADDRESS; }
.;
%%
