#!/bin/bash

# provedeni nejakych akci
./pdscli -f rules > /dev/null
./pdscli -a 35 allow icmp from 185.8.239.145 to any > /dev/null
./pdscli -d 30 > /dev/null

# vystupy k porovnani
echo "Output:"
./pdscli -p

echo "Correct output:"
echo "id	action	srcip		srcport	dstip		dstport	protocol
10	allow	147.229.1.1	*	*	80	tcp
20	allow	*	53	8.8.8.8	*	udp
35	allow	185.8.239.145	*	*	*	icmp
40	deny	*	*	*	*	ip"

#vycisteni pravidel firewallu
./pdscli -d 10 -d 20 -d 40 -d 35 > /dev/null
