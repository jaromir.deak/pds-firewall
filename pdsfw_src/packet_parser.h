/*
 * Simple firewall - kernel module
 * Project for course Data Communications, Computer Networks and Protocols (PDS)
 * Faculty of Information Technology, Brno University of Technology
 *
 * Author: Jaromir Deak, xdeakj00@stud.fit.vutbr.cz
 * Date: April 2015
 */

#ifndef PACKET_PARSER_H_
#define PACKET_PARSER_H_

#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/udp.h>
#include <linux/tcp.h>
#include <linux/skbuff.h>
#include <linux/ip.h>

/* Struktura pro uchovani testovaneho packetu */
typedef struct
{
  unsigned int src_ip;        //
  unsigned int src_port;        //0~2^32
  unsigned int dest_ip;
  unsigned int dest_port;
  unsigned char proto;        //0: ip, 1: tcp, 2: udp
}Packet;

/**
 * Prevod ip adresy na retezec
 */
void ip_hl_to_str(unsigned int ip, char *ip_str);

/**
 * Prevod retezce na ip adresu
 */
unsigned int ip_str_to_hl(char *ip_str);

/*
 * Ziska data o packetu
 */
void parse_packet(Packet * packet, struct sk_buff *skb);

#endif /* PACKET_PARSER_H_ */
