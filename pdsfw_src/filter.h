/*
 * Simple firewall - kernel module
 * Project for course Data Communications, Computer Networks and Protocols (PDS)
 * Faculty of Information Technology, Brno University of Technology
 *
 * Author: Jaromir Deak, xdeakj00@stud.fit.vutbr.cz
 * Date: April 2015
 */

#ifndef FILTER_H_
#define FILTER_H_

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/skbuff.h>
#include <linux/list.h> // Implementace vazaneho seznamu

#include "packet_parser.h"

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("PDS firewall.");
MODULE_AUTHOR("Jaromir Deak");

#define DENY 0
#define ALLOW 1

#define IP 0
#define ICMP 1
#define TCP 6
#define UDP 17


/* Struktura pro uchovani filtrovaciho pravidla */
struct rule
{
  unsigned int id;
  char id_str[16];
  unsigned int src_ip;
  char src_ip_str[16];
  unsigned int src_port;
  char src_port_str[16];
  unsigned int dest_ip;
  char dest_ip_str[16];
  unsigned int dest_port;
  char dest_port_str[16];
  unsigned char proto;
  char proto_str[16];
  unsigned char action;
  char action_str[16];
  struct list_head list;
};

struct mf_rule_desp
{
  char *id;
  char *src_ip;
  char *src_port;
  char *dest_ip;
  char *dest_port;
  char *proto;
  char *action;
};

/* Vytvori a prida pravidlo do listu pravidel */
void add_rule(struct rule * policy_list, struct mf_rule_desp * rule_desp);

/*
 * Smaze pravidlo definovane pomoci id
 */
void delete_rule(struct rule * policy_list, int id);

/*
 * Smaze vsechna pravidla
 */
void clean_policy_list(struct rule * policy_list);

/* Vypise seznam aktualne pouzivanych pravidel */
void print_policy_list(struct rule * policy_list);

/*
 * Vypise pravidlo
 */
void print_rule(struct rule * rule);

/*
 * Vrati odkaz na pravidlo v textovem retezci
 */
char * get_rule_str(struct rule * rule);

/*
 * Vypise podrobnosti o packetu
 */
void print_packet(Packet * packet);

/*
 * Povoli nebo zamitne packet
 */
int check_packet(struct rule * policy_list, Packet * packet);

void init_mf_rule_desp(struct mf_rule_desp* a_rule_desp);

unsigned int str_to_uint(char *str);

#endif /* FILTER_H_ */
