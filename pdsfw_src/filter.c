/*
 * Simple firewall - kernel module
 * Project for course Data Communications, Computer Networks and Protocols (PDS)
 * Faculty of Information Technology, Brno University of Technology
 *
 * Author: Jaromir Deak, xdeakj00@stud.fit.vutbr.cz
 * Date: April 2015
 * 
 * Used:
 * http://www.roman10.net/a-linux-firewall-using-netfilter-part-1overview/ (whole tutorial)
 * http://wiki.tldp.org/lkmpg/en/content/ch05/2_6
 */

#include "filter.h"

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("PDS firewall.");
MODULE_AUTHOR("Jaromir Deak");

void add_rule(struct rule * policy_list, struct mf_rule_desp * rule_desp)
{
  struct rule* new_rule_ptr; // pointer na nove pravidlo
  new_rule_ptr = kmalloc(sizeof(*new_rule_ptr), GFP_KERNEL);  // alokace noveho pravidla

  /* Naplneni hodnot noveho pravidla */
  /* id */
  strcpy(new_rule_ptr->id_str, rule_desp->id);
  new_rule_ptr->id = str_to_uint(rule_desp->id);

  /* src_ip */
  strcpy(new_rule_ptr->src_ip_str, rule_desp->src_ip);
  if(rule_desp->src_ip[0] == '*')
    new_rule_ptr->src_ip = 0;
  else
    new_rule_ptr->src_ip = ip_str_to_hl(rule_desp->src_ip);

  /* src_port */
  strcpy(new_rule_ptr->src_port_str, rule_desp->src_port);
  if(rule_desp->src_port[0] == '*')
    new_rule_ptr->src_port = 0;
  else
    new_rule_ptr->src_port = str_to_uint(rule_desp->src_port);

  /* dest_ip */
  strcpy(new_rule_ptr->dest_ip_str, rule_desp->dest_ip);
  if(rule_desp->dest_ip[0] == '*')
    new_rule_ptr->dest_ip = 0;
  else
    new_rule_ptr->dest_ip = ip_str_to_hl(rule_desp->dest_ip);

  /* dest_port */
  strcpy(new_rule_ptr->dest_port_str, rule_desp->dest_port);
  if(rule_desp->dest_port[0] == '*')
    new_rule_ptr->dest_port = 0;
  else
    new_rule_ptr->dest_port = str_to_uint(rule_desp->dest_port);

  /* proto */
  strcpy(new_rule_ptr->proto_str, rule_desp->proto);
  if(rule_desp->proto[0] == '*')
      new_rule_ptr->proto = 0;
  else if(strcmp(rule_desp->proto, "tcp") == 0)
      new_rule_ptr->proto = TCP;
  else if(strcmp(rule_desp->proto, "udp") == 0)
        new_rule_ptr->proto = UDP;
  else if(strcmp(rule_desp->proto, "icmp") == 0)
        new_rule_ptr->proto = ICMP;
  else if(strcmp(rule_desp->proto, "ip") == 0)
        new_rule_ptr->proto = IP;

  /* action */
  strcpy(new_rule_ptr->action_str, rule_desp->action);
  if(strcmp(rule_desp->action, "allow") == 0)
        new_rule_ptr->action = ALLOW;

  INIT_LIST_HEAD(&(new_rule_ptr->list));

  /* Vlozeni pravidla do listu */
  struct list_head *p, *q; // ukazatel
  struct rule * rule_ptr;

  list_for_each_safe(p, q, &(policy_list->list))
  {
    rule_ptr = list_entry(p, struct rule, list);

    /* Vlozeni pravidla pred aktualni pokud aktualni ma mensi id */
    if(new_rule_ptr->id < rule_ptr->id)
    {
      //printk(KERN_INFO "insert a rule: %d before rule id: %d", new_rule_ptr->id, rule_ptr->id);
      list_add_tail(&(new_rule_ptr->list), p);
      return;
    }

    /* Nahrazeni pravidla se stejnym id */
    if(rule_ptr->id == new_rule_ptr->id)
    {
      //printk(KERN_INFO "replace a rule: %d, by id: %d", rule_ptr->id, new_rule_ptr->id);
      list_replace(p, &(new_rule_ptr->list));
      return;
    }

  }
  list_add_tail(&(new_rule_ptr->list), &(policy_list->list));
}

void delete_rule(struct rule * policy_list, int id)
{
  struct list_head *p, *q;
  struct rule * rule_ptr;

  list_for_each_safe(p, q, &(policy_list->list))
  {
    rule_ptr = list_entry(p, struct rule, list);
    //printk(KERN_INFO "delete a rule: %d, found: %d", id , rule_ptr->id);
    if(rule_ptr->id == id)
    {
      //printk(KERN_INFO "PDSFW: delete rule id %d\n", rule_ptr->id);
      list_del(p);
      kfree(rule_ptr);
      return;
    }
  }
}

void clean_policy_list(struct rule * policy_list)
{
  struct list_head * list_head_ptr; // ukazatel
  struct rule * rule_ptr;

  list_for_each(list_head_ptr, &(policy_list->list))
  {
    rule_ptr = list_entry(list_head_ptr, struct rule, list);
    list_del(list_head_ptr);
    kfree(rule_ptr);
  }
}

void print_policy_list(struct rule * policy_list)
{
  struct list_head * list_head_ptr; // ukazatel
  struct rule * rule_ptr;

  list_for_each(list_head_ptr, &(policy_list->list))
  {
    rule_ptr = list_entry(list_head_ptr, struct rule, list);
    print_rule(rule_ptr);
  }
}

void print_rule(struct rule * rule)
{
  printk(KERN_INFO "PDSFW: rule: %d %s %s %s %s %s %s\n",
         rule->id,
         rule->action_str,
         rule->src_ip_str,
         rule->src_port_str,
         rule->dest_ip_str,
         rule->dest_port_str,
         rule->proto_str);
}

char * get_rule_str(struct rule * rule)
{
  char * rule_str = kmalloc(122 * sizeof(char), GFP_KERNEL);
  sprintf(rule_str, "%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
         rule->id_str,
         rule->action_str,
         rule->src_ip_str,
         rule->src_port_str,
         rule->dest_ip_str,
         rule->dest_port_str,
         rule->proto_str);

  return rule_str;
}

void print_packet(Packet * packet)
{
  char src_ip_str[16], dest_ip_str[16];

  ip_hl_to_str(packet->src_ip, src_ip_str);
  ip_hl_to_str(packet->dest_ip, dest_ip_str);

  printk(KERN_INFO "PDSFW: packet: %s %d %s %d %d\n",
         src_ip_str,
         packet->src_port,
         dest_ip_str,
         packet->dest_port,
         packet->proto);
}

int check_packet(struct rule * policy_list, Packet * packet)
{
  struct list_head *p;
  struct rule *rule;

  print_packet(packet);

  list_for_each(p, &(policy_list->list))
  {
    rule = list_entry(p, struct rule, list);
    //print_rule(rule);

    // Kontrola IP odesilatele
    if(rule->src_ip != 0 && rule->src_ip != packet->src_ip)
      continue;

    // Kontrola IP prijemce
    if(rule->dest_ip != 0 && rule->dest_ip != packet->dest_ip)
      continue;

    // Kontrola portu odesilatele
    if(rule->src_port != 0 && rule->src_port != packet->src_port)
      continue;

    // Kontrola portu prijemce
    if(rule->dest_port != 0 && rule->dest_port != packet->dest_port)
      continue;

    // Kontrola protokolu
    if(rule->proto != 0 && rule->proto != packet->proto)
      continue;

    //printk(KERN_INFO "proto p: %d r: %d", packet->proto, rule->proto);

    if(rule->action == ALLOW)
    {
      //printk(KERN_INFO " ALLOW rule: %d\n", rule->id);
      return NF_ACCEPT;
    }
    else
    {
      //printk(KERN_INFO " DENY rule: %d\n", rule->id);
      return NF_DROP;
    }
  }

  return NF_ACCEPT;
}

void init_mf_rule_desp(struct mf_rule_desp* a_rule_desp)
{
  a_rule_desp->id = (char *)kmalloc(16, GFP_KERNEL);
  a_rule_desp->src_ip = (char *)kmalloc(16, GFP_KERNEL);
  a_rule_desp->src_port = (char *)kmalloc(16, GFP_KERNEL);
  a_rule_desp->dest_ip = (char *)kmalloc(16, GFP_KERNEL);
  a_rule_desp->dest_port = (char *)kmalloc(16, GFP_KERNEL);
  a_rule_desp->proto = (char *)kmalloc(16, GFP_KERNEL);
  a_rule_desp->action =  (char *)kmalloc(16, GFP_KERNEL);
}

unsigned int str_to_uint(char *str)
{
  int index = 0;
  unsigned int result = 0;

  while(str[index] != '\0')
  {
    result = result * 10 + (str[index] - '0');
    ++index;
  }

  return result;
}
