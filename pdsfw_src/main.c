/*
 * Simple firewall - kernel module
 * Project for course Data Communications, Computer Networks and Protocols (PDS)
 * Faculty of Information Technology, Brno University of Technology
 *
 * Author: Jaromir Deak, xdeakj00@stud.fit.vutbr.cz
 * Date: April 2015
 */

#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/proc_fs.h>
#include <asm/uaccess.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/udp.h>
#include <linux/tcp.h>
#include <linux/skbuff.h>
#include <linux/ip.h>
#include <linux/list.h> // Implementace vazaneho seznamu

#include "filter.h"
#include "packet_parser.h"

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION("PDS firewall.");
MODULE_AUTHOR("Jaromir Deak");

#define PROCF_MAX_SIZE 1024
#define PROCF_NAME "pdsfw"

/* List pravidel filtru */
static struct rule policy_list;

/* Struktury pro registraci odchytavani */
static struct nf_hook_ops nfho_in;  // odchyt prichozich paketu

/* Struktury pro nastaveni procfs */
unsigned long procf_buffer_pos;
char *procf_buffer;
struct proc_dir_entry *proc_file_entry;
int read_enabled;

/**
 * Prijem dat od klienta
 */
int procf_write(struct file *file, const char *buffer, unsigned long count, void *data)
{
  //printk(KERN_INFO "PDSFW: write callback\n");
  int i, j;
  struct mf_rule_desp *rule_desp;

  /* Inicializace cteni zapisovanych dat */
  /* Nastaveni ukazatele do bufferu na zacatek */
  procf_buffer_pos = 0;
  /* Omezeni delky ctenych dat tak aby se vlezla do bufferu */
  if (procf_buffer_pos + count > PROCF_MAX_SIZE)
  {
    count = PROCF_MAX_SIZE - procf_buffer_pos;
  }
  /* Kopirovani dat z user space do bufferu */
  if (copy_from_user(procf_buffer + procf_buffer_pos, buffer, count))
  {
    return -EFAULT;
  }

  /* Cteni a zpracovani */
  /* Prikaz ke smazani pravidla */
  if (procf_buffer[procf_buffer_pos] == 'd')
  {
    i = procf_buffer_pos + 2;
    j = 0;
    while ((procf_buffer[i] != ' ') && (procf_buffer[i] != '\n'))
    {
      j = j * 10 + (procf_buffer[i] - '0');
      ++i;
    }
    delete_rule(&policy_list, j);

    return count;
  }

  /* Prikaz pro vypis vsech pravidel */
  if (procf_buffer[procf_buffer_pos] == 'p')
  {
    //print_policy_list(&policy_list);
    read_enabled = 1;
    return count;
  }

  /* Prikaz pro pridani pravidla */
  if (procf_buffer[procf_buffer_pos] == 'a')
  {
    rule_desp = kmalloc(sizeof(*rule_desp), GFP_KERNEL);
    if (rule_desp == NULL)
    {
      printk(KERN_INFO "PDSFW: E: cannot allocate memory for rule_desp\n");
      return -ENOMEM;
    }
    init_mf_rule_desp(rule_desp);

    i = procf_buffer_pos + 2;

    /***id***/
    j = 0;
    while (procf_buffer[i] != ' ')
    {
      rule_desp->id[j++] = procf_buffer[i++];
    }
    ++i;
    rule_desp->id[j] = '\0';
    //printk(KERN_INFO "id: %s\n", rule_desp->id);

    /***action***/
    j = 0;
    while (procf_buffer[i] != ' ')
    {
      rule_desp->action[j++] = procf_buffer[i++];
    }
    ++i;
    rule_desp->action[j] = '\0';
    //printk(KERN_INFO "action: %s\n", rule_desp->action);

    /***proto***/
    j = 0;
    while (procf_buffer[i] != ' ')
    {
      rule_desp->proto[j++] = procf_buffer[i++];
    }
    ++i;
    rule_desp->proto[j] = '\0';
    //printk(KERN_INFO "proto: %s\n", rule_desp->proto);

    /***src ip***/
    j = 0;
    while (procf_buffer[i] != ' ')
    {
      rule_desp->src_ip[j++] = procf_buffer[i++];
    }
    ++i;
    rule_desp->src_ip[j] = '\0';
    //printk(KERN_INFO "src ip: %s\n", rule_desp->src_ip);

    /***src port number***/
    j = 0;
    while (procf_buffer[i] != ' ')
    {
      rule_desp->src_port[j++] = procf_buffer[i++];
    }
    ++i;
    rule_desp->src_port[j] = '\0';
    //printk(KERN_INFO "src_port: %s\n", rule_desp->src_port);

    /***dest ip***/
    j = 0;
    while (procf_buffer[i] != ' ')
    {
      rule_desp->dest_ip[j++] = procf_buffer[i++];
    }
    ++i;
    rule_desp->dest_ip[j] = '\0';
    //printk(KERN_INFO "dest ip: %s\n", rule_desp->dest_ip);

    /***src port number***/
    j = 0;
    while (procf_buffer[i] != ' ' && procf_buffer[i] != '\n')
    {
      rule_desp->dest_port[j++] = procf_buffer[i++];
    }
    ++i;
    rule_desp->dest_port[j] = '\0';
    //printk(KERN_INFO "dest_port: %s\n", rule_desp->dest_port);

    add_rule(&policy_list, rule_desp);
    kfree(rule_desp);
    return count;
  }

  return count;
}

/**
 * Prevod cisla portu na retezec
 */
void port_int_to_str(unsigned int port, char *port_str)
{
  sprintf(port_str, "%u", port);
}

/**
 * Odeslani dat klientske aplikaci
 */
static ssize_t procRead(struct file *fp, char *buffer, size_t len, loff_t *offset)
{
  int ret = 0;
  struct list_head * list_head_ptr;
  struct rule * rule;
  char * rule_str;

  if (read_enabled == 0)
  {
    //printk(KERN_INFO "PDSFW: Last read callback\n");
    return 0;
  }
  else
    read_enabled = 0;

  //printk(KERN_INFO "PDSFW: read callback\n");

  list_for_each(list_head_ptr, &policy_list.list)
  {
    rule = list_entry(list_head_ptr, struct rule, list);

    rule_str = get_rule_str(rule); // vytvoreni retezce pravidla

    memcpy(procf_buffer + procf_buffer_pos, rule_str, strlen(rule_str));
    procf_buffer_pos += strlen(rule_str);

    ret += strlen(rule_str);
    kfree(rule_str);
  }

  copy_to_user(buffer, procf_buffer, procf_buffer_pos);

  return ret;
}

static const struct file_operations proc_file_fops = {.owner = THIS_MODULE, .write = procf_write, .read = procRead, };

/**
 *  Odchytavaci funkce: registrovana pro prichozi pakety
 */
unsigned int hook_func_in(unsigned int hooknum, struct sk_buff *skb, const struct net_device *in,
                          const struct net_device *out, int (*okfn)(struct sk_buff *))
{
  Packet packet;  // promenna pro ulozeni dat o prochazejicim packetu

  /* Zjisteni informaci o packetu */
  parse_packet(&packet, skb);

  /* Provereni packetu */
  return check_packet(&policy_list, &packet);
}

/**
 * Inicializacni funkce.
 */
int init_module()
{
  printk(KERN_INFO "PDSFW: initializing firewall\n");

  /* Pripojeni procfs */
  procf_buffer = (char *)vmalloc(PROCF_MAX_SIZE);
  proc_file_entry = proc_create(PROCF_NAME, 0777, NULL, &proc_file_fops);       //TODO: upravit prava
  if (proc_file_entry == NULL)
    return -ENOMEM;
  printk(KERN_INFO "PDSFW: /proc/%s is created\n", PROCF_NAME);

  /* Vytvoreni listu pravidel filtru */
  INIT_LIST_HEAD(&(policy_list.list));

  /* Napleni struktury pro zachytavani prichozich paketu */
  nfho_in.hook = hook_func_in;
  nfho_in.hooknum = NF_INET_LOCAL_IN;
  nfho_in.pf = PF_INET;
  nfho_in.priority = NF_IP_PRI_FIRST;
  /* Registrace odchytavani prichozich paketu */
  nf_register_hook(&nfho_in);

  printk(KERN_INFO "PDSFW: firewall activated\n");

  return 0;
}

/**
 * Funkce pro odstraneni modulu z jadra.
 */
void cleanup_module()
{
  /* Odebrani procfs ze systemu */
  remove_proc_entry(PROCF_NAME, NULL);

  /* Vycisteni seznamu pravidel */
  print_policy_list(&policy_list);

  /* Odregistrovani odc
   * hytavani prichozich paketu */
  nf_unregister_hook(&nfho_in);

printk(KERN_INFO "PDSFW: firewall deactivated\n");
}
