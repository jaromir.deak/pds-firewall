/*
 * Simple firewall - kernel module
 * Project for course Data Communications, Computer Networks and Protocols (PDS)
 * Faculty of Information Technology, Brno University of Technology
 *
 * Author: Jaromir Deak, xdeakj00@stud.fit.vutbr.cz
 * Date: April 2015
 */

#include "packet_parser.h"

/**
 * Prevod ip adresy na retezec
 */
void ip_hl_to_str(unsigned int ip, char *ip_str)
{
  /*convert hl to byte array first*/
  unsigned char ip_array[4];
  memset(ip_array, 0, 4);
  ip_array[0] = (ip_array[0] | (ip >> 24));
  ip_array[1] = (ip_array[1] | (ip >> 16));
  ip_array[2] = (ip_array[2] | (ip >> 8));
  ip_array[3] = (ip_array[3] | ip);
  sprintf(ip_str, "%u.%u.%u.%u", ip_array[0], ip_array[1], ip_array[2], ip_array[3]);
}

/**
 * Prevod retezce na ip adresu
 */
unsigned int ip_str_to_hl(char *ip_str)
{
  /*convert the string to byte array first, e.g.: from "131.132.162.25" to [131][132][162][25]*/
  unsigned char ip_array[4];
  int i = 0;
  unsigned int ip = 0;
  if (ip_str == NULL)
  {
    return 0;
  }
  memset(ip_array, 0, 4);
  while (ip_str[i] != '.')
  {
    ip_array[0] = ip_array[0] * 10 + (ip_str[i++] - '0');
  }
  ++i;
  while (ip_str[i] != '.')
  {
    ip_array[1] = ip_array[1] * 10 + (ip_str[i++] - '0');
  }
  ++i;
  while (ip_str[i] != '.')
  {
    ip_array[2] = ip_array[2] * 10 + (ip_str[i++] - '0');
  }
  ++i;
  while (ip_str[i] != '\0')
  {
    ip_array[3] = ip_array[3] * 10 + (ip_str[i++] - '0');
  }
  /*convert from byte array to host long integer format*/
  ip = (ip_array[0] << 24);
  ip = (ip | (ip_array[1] << 16));
  ip = (ip | (ip_array[2] << 8));
  ip = (ip | ip_array[3]);
  //printk(KERN_INFO "ip_str_to_hl convert %s to %u\n", ip_str, ip);
  return ip;
}

void parse_packet(Packet * packet, struct sk_buff *skb)
{
  /*get src address, src netmask, src port, dest ip, dest netmask, dest port, protocol*/
    struct iphdr *ip_header = (struct iphdr *)skb_network_header(skb);
    struct udphdr *udp_header;
    struct tcphdr *tcp_header;
    //char src_ip_str[16], dest_ip_str[16];

    /**get src and dest ip addresses**/
    unsigned int src_ip = ntohl((unsigned int)ip_header->saddr);
    unsigned int dest_ip = ntohl((unsigned int)ip_header->daddr);
    unsigned int src_port = 0;
    unsigned int dest_port = 0;

    /***get src and dest port number***/
    if (ip_header->protocol == 17)
    {
      udp_header = (struct udphdr *)(skb_transport_header(skb) + 20);
      src_port = (unsigned int)ntohs(udp_header->source);
      dest_port = (unsigned int)ntohs(udp_header->dest);
    }
    else if (ip_header->protocol == 6)
    {
      tcp_header = (struct tcphdr *)(skb_transport_header(skb) + 20);
      src_port = (unsigned int)ntohs(tcp_header->source);
      dest_port = (unsigned int)ntohs(tcp_header->dest);
    }

    // IP adresa ve forme retezce
    //ip_hl_to_str(ntohl(src_ip), src_ip_str);
    //ip_hl_to_str(ntohl(dest_ip), dest_ip_str);

    packet->src_ip = src_ip;
    packet->dest_ip = dest_ip;
    packet->src_port = src_port;
    packet->dest_port = dest_port;
    packet->proto = ip_header->protocol;
}
