# Soubor: Makefile
# Autor: xdeakj00
# Datum: 20.04.2015

.PHONY: all clean

CC = g++

CFLAGS = -lfl #--std=c99 -Wall -Wextra -pedantic -O3 -lm

SOURCES = pdscli_src/main.cpp pdscli_src/analyze.tab.c pdscli_src/lex.yy.c pdscli_src/Rule.cpp

HEADERS = pdscli_src/Rule.h pdscli_src/analyze.tab.h

obj-m += pdsfw_src/pdsfw.o 
pdsfw-objs += pdsfw_src/main.o pdsfw_src/filter.o pdsfw_src/packet_parser.o

all: pdscli pdsfw

analyze:
	bison -d pdscli_src/analyze.y
	flex pdscli_src/analyze.l

pdscli: $(SOURCES) $(HEADERS)
	$(CC) $(SOURCES) -o $@ $(CFLAGS)

pdsfw:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD)/pdsfw_src modules
	cp pdsfw_src/pdsfw.ko ./

clean:
	rm pdscli
	rm pdsfw.ko
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD)/pdsfw_src clean
